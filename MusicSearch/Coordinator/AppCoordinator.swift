//
//  AppCoordinator.swift
//  MusicSearch
//
//  Created by Andrey Savitskiy on 20.03.2020.
//  Copyright © 2020 com.adsv.MusicSearch. All rights reserved.
//

import UIKit

class AppCoordinator {

    var navigationController: UINavigationController

    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }

    func start() {
        let vc = TracksListViewController.instantiate()
        vc.appCoordinator = self
        self.navigationController.pushViewController(vc, animated: false)
    }

    func showDetailsFor(track: SoundTrackModel) {
        let vc = DetailViewController.instantiate()
        vc.appCoordinator = self
        vc.configureWith(model: track)
        self.navigationController.pushViewController(vc, animated: true)
    }

}
