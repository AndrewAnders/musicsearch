//
//  Storyborded.swift
//  MusicSearch
//
//  Created by Andrey Savitskiy on 20.03.2020.
//  Copyright © 2020 com.adsv.MusicSearch. All rights reserved.
//

import UIKit

protocol Storyborded {
    static func instantiate() -> Self
}

extension Storyborded where Self: UIViewController {
    static func instantiate() -> Self {
        let id = String(describing: self)
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        return storyboard.instantiateViewController(withIdentifier: id) as! Self
    }
}
