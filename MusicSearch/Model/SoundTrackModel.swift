//
//  SoundTrack.swift
//  MusicSearch
//
//  Created by Andrey Savitskiy on 18.03.2020.
//  Copyright © 2020 com.AndrewSV.MusicSearch. All rights reserved.
//

import Foundation

protocol SoundTrackPresentable {
    var trackId: UInt { get }
    var title: String { get }
    var type: ItemType { get }
    var artist_title: String { get }
    var cover_image: String { get }
}

class SoundTrackModel: SoundTrackPresentable {
    var trackId: UInt
    var title: String
    var type: ItemType
    var duration: UInt
    var rank: Int
    var artist: Artist
    var album: Album
    var artist_title: String
    var cover_image: String

    required init(trackId: UInt, title: String, type: ItemType, duration: UInt, rank: Int, artist: Artist, album: Album) {
        self.trackId = trackId
        self.title = title
        self.type = type
        self.duration = duration
        self.rank = rank
        self.artist = artist
        self.album = album
        self.artist_title = artist.name
        self.cover_image = album.cover_small
    }
}

struct Album {
    var title: String
    var cover_small: String
    var cover_big: String
}

struct Artist {
    var name: String
    var picture_small: String
}

enum ItemType {
    case track
    case album
}
