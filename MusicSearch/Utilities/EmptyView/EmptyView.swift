//
//  EmptyView.swift
//  MusicSearch
//
//  Created by Andrey Savitskiy on 20.03.2020.
//  Copyright © 2020 com.adsv.MusicSearch. All rights reserved.
//

import UIKit

class EmptyView: UIView {

    @IBOutlet weak var txtIcon: UILabel!
    @IBOutlet weak var txtMessage: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!

    static func fromNib() -> Self {
        let bundle = Bundle(for: self)
        let nib = bundle.loadNibNamed(String(describing: self), owner: self, options: nil)
        return nib!.first as! Self
    }

    // MARK: - Show messages
    func messageEmpty() {
        configureDefault()
        txtMessage.text = "Type something\nto find music"
    }

    func messageNoResults(searchRequest: String) {
        configureDefault()
        txtMessage.text = "No results for your request \(searchRequest)"
    }

    func messageSearch() {
        configureProgress()
        txtMessage.text = "Searching..."
    }

    func errorMessage(error: Error) {
        configureError()
        if case ApiError.noConnection = error {
            txtMessage.text = "\("No internet connection")"
        } else {
            txtMessage.text = "\(error.localizedDescription)"
        }
    }

    // MARK: - Private methods
    private func configureDefault() {
        activityIndicator.stopAnimating()
        txtIcon.text = "🔎"
    }

    private func configureProgress() {
        activityIndicator.startAnimating()
        txtIcon.text = "🔎"
    }

    private func configureError() {
        activityIndicator.stopAnimating()
        txtIcon.text = "⚠️"
    }

}
