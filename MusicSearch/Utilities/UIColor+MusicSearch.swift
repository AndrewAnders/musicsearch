//
//  UIColor+MusicSearch.swift
//  MusicSearch
//
//  Created by Andrey Savitskiy on 20.03.2020.
//  Copyright © 2020 com.adsv.MusicSearch. All rights reserved.
//

import UIKit

extension UIColor {

    static func borderColor() -> UIColor {
        UIColor(red: 212/255, green: 212/255, blue: 212/255, alpha: 1)
    }

}
