//
//  ViewController.swift
//  MusicSearch
//
//  Created by Andrey Savitskiy on 17.03.2020.
//  Copyright © 2020 com.AndrewSV.MusicSearch. All rights reserved.
//

import UIKit

protocol TracksListViewProtocol: class {
    func updateItemsList()
    func showError(error: Error)
}

class TracksListViewController: UIViewController, Storyborded {

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!

    weak var appCoordinator: AppCoordinator?
    let cellIdentifier = "soundTrackCell"
    var viewModel: TracksListViewModel?
    var searchTimer: Timer?
    var emptyView: EmptyView?

    override func viewDidLoad() {
        super.viewDidLoad()

        let nib = UINib(nibName: "SoundTrackCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: cellIdentifier)
        tableView.dataSource = self
        tableView.delegate = self
        tableView.separatorInset = UIEdgeInsets(top: 0, left: 78, bottom: 0, right: 20)
        searchBar.delegate = self
        tableView.keyboardDismissMode = .interactive

        emptyView = EmptyView.fromNib()
        tableView.backgroundView = emptyView
        emptyView?.messageEmpty()

        viewModel = TracksListViewModel(view: self)
    }

    override func viewDidAppear(_ animated: Bool) {
        searchBar.becomeFirstResponder()
    }
}

extension TracksListViewController: UITableViewDataSource, UITableViewDelegate {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let count = viewModel?.tracks().count, count != 0 {
            tableView.separatorStyle = .singleLine
            tableView.backgroundView?.isHidden = true
            return count
        } else {
            if let searchText = searchBar.text, !searchText.isEmpty {
                emptyView?.messageNoResults(searchRequest: searchText)
            } else {
                emptyView?.messageEmpty()
            }
            tableView.separatorStyle = .none
            tableView.backgroundView?.isHidden = false
            return 0
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? SoundTrackCell
        let itemViewModel = viewModel?.tracks()[indexPath.row]
        cell?.configure(withSoundTrackModel: itemViewModel!)
        return cell!
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        54
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let model = viewModel else {
            return
        }
        appCoordinator?.showDetailsFor(track: model.trackFor(index: indexPath.row))
        tableView.deselectRow(at: indexPath, animated: true)
    }

}

extension TracksListViewController: TracksListViewProtocol {

    func updateItemsList() {
        tableView.reloadData()
    }

    func showError(error: Error) {
        viewModel?.clearTracks()
        tableView.reloadData()
        emptyView?.errorMessage(error: error)
    }
}

extension TracksListViewController: UISearchBarDelegate {

    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        searchTimer?.invalidate()
        searchTimer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: false, block: { [weak self] timer in
            self?.viewModel?.performSearch(searchText: searchText)
        })
    }

}
