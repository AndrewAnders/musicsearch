//
//  DetailViewController.swift
//  MusicSearch
//
//  Created by Andrey Savitskiy on 20.03.2020.
//  Copyright © 2020 com.adsv.MusicSearch. All rights reserved.
//

import UIKit

protocol DetailPresenterProtocol: class {
    func presentImage(image: UIImage)
}

class DetailViewController: UIViewController, Storyborded {

    @IBOutlet weak var coverImage: UIImageView!
    @IBOutlet weak var txtTrackTitle: UILabel!
    @IBOutlet weak var txtArtistTitle: UILabel!
    @IBOutlet weak var txtAlbumTitle: UILabel!
    @IBOutlet weak var txtDuration: UILabel!
    @IBOutlet weak var txtRank: UILabel!
    @IBOutlet weak var txtType: UILabel!

    weak var appCoordinator: AppCoordinator?
    var viewModel: DetailViewModel?
    private var trackModel: SoundTrackModel?

    func configureWith(model: SoundTrackModel) {
        trackModel = model
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        guard let model = trackModel else {
            return
        }

        viewModel = DetailViewModel(view: self)
        viewModel?.getImageFor(trackModel: model)

        txtTrackTitle.text = model.title
        txtArtistTitle.text = model.artist.name
        txtAlbumTitle.text = "📀 \(model.album.title)"

        let (_, m, s) = SecondsConverter.secondsToHoursMinutesSeconds(seconds: Int(model.duration))
        txtDuration.text = "Duration: \(String(format: "%02d:%02d", m, s))"

        txtRank.text = "Rank: \(model.rank)"

        switch model.type {
        case .album:
            txtType.text = "Album 📀"
        case .track:
            txtType.text = "SoundTrack 🎵"
        }
    }

}

extension DetailViewController: DetailPresenterProtocol {

    func presentImage(image: UIImage) {
        coverImage.image = image
    }

}
