//
//  SoundTrackCell.swift
//  MusicSearch
//
//  Created by Andrey Savitskiy on 19.03.2020.
//  Copyright © 2020 com.AndrewSV.MusicSearch. All rights reserved.
//

import UIKit

class SoundTrackCell: UITableViewCell {

    @IBOutlet weak var coverImage: UIImageView!
    @IBOutlet weak var txtTitle: UILabel!
    @IBOutlet weak var txtSubTitle: UILabel!
    @IBOutlet weak var txtType: UILabel!
    let imageService = ImageService.sharedInstance

    override func awakeFromNib() {
        super.awakeFromNib()

        coverImage.layer.borderColor = UIColor.borderColor().cgColor
        coverImage.layer.borderWidth = 0.3;
    }

    func configure(withSoundTrackModel soundTrackModel: SoundTrackPresentable) {
        coverImage.image = UIImage(named: "media_icon")
        txtTitle.text = soundTrackModel.title
        txtSubTitle.text = soundTrackModel.artist_title

        switch soundTrackModel.type {
        case .track:
            txtType.text = "🎵"
        case .album:
            txtType.text = "📀"
        }

        imageService.download(imageURL: soundTrackModel.cover_image) { (result) in
            if case .success(let image) = result {
                self.coverImage.image = image
            }
        }
    }
    
}
