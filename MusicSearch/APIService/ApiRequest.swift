//
//  ApiRequest.swift
//  MusicSearch
//
//  Created by Andrey Savitskiy on 19.03.2020.
//  Copyright © 2020 com.AndrewSV.MusicSearch. All rights reserved.
//

import Foundation
import Alamofire

protocol ApiRequestProtocol {
    func url() -> URL
    func getHeaders() -> HTTPHeaders
    func parameters() -> Parameters
}

class ApiSearchRequest: ApiRequestProtocol {

    private var searchRequest: String

    init(search: String) {
        searchRequest = search
    }

    func getHeaders() -> HTTPHeaders {
        [
        "x-rapidapi-host": Configuration.ApiHost,
        "x-rapidapi-key": Configuration.ApiKey,
        ]
    }

    func parameters() -> Parameters {
        [ "q": searchRequest ]
    }

    func url() -> URL {
        URLGenerator.musicAPIUrl(withPath: "search")
    }
}
