//
//  URLGenerator.swift
//  MusicSearch
//
//  Created by Andrey Savitskiy on 18.03.2020.
//  Copyright © 2020 com.AndrewSV.MusicSearch. All rights reserved.
//

import UIKit

class URLGenerator {

    static let baseURL: String = "https://deezerdevs-deezer.p.rapidapi.com"

    class func musicAPIUrl(withPath path: String) -> URL {
        let urlString = URLGenerator.baseURL + "/" + path
        return URL(string: urlString)!
    }

}
