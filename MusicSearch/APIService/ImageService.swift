//
//  ImageService.swift
//  MusicSearch
//
//  Created by Andrey Savitskiy on 20.03.2020.
//  Copyright © 2020 com.adsv.MusicSearch. All rights reserved.
//

import Alamofire
import AlamofireImage

class ImageService {

    static let sharedInstance = ImageService()

    let imageDownloader: ImageDownloader
    let imageCache = AutoPurgingImageCache()

    private init() {
        imageDownloader = ImageDownloader()
    }

    func download(imageURL: String, completion: @escaping (Swift.Result<UIImage, Error>) -> Void) {
        if let cachedImage = imageCache.image(withIdentifier: imageURL) {
            completion(.success(cachedImage))
            return
        }

        let request = URLRequest(url: URL(string: imageURL)!)
        imageDownloader.download(request) { response in
            switch response.result {
            case .success(let image):
                self.imageCache.add(image, withIdentifier: imageURL)
                completion(.success(image))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }

}
