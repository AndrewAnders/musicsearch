//
//  SoundTracksManager.swift
//  MusicSearch
//
//  Created by Andrey Savitskiy on 18.03.2020.
//  Copyright © 2020 com.AndrewSV.MusicSearch. All rights reserved.
//

import UIKit

class SoundTracksManager {

    var trackDictionary: NSDictionary

    required init(trackDictionary: NSDictionary) {
        self.trackDictionary = trackDictionary
    }

    func serialize() -> [SoundTrackModel] {
        var soundtracks = [SoundTrackModel]()

        if let dataDictionary = trackDictionary["data"] as? NSArray {

            dataDictionary.forEach { trackData in
                var artist: Artist? = nil
                var album: Album? = nil
                var type: ItemType? = nil

                if let value = trackData as? NSDictionary {
                    let trackId = value["id"] as! UInt
                    let title = value["title"] as! String
                    let rank = value["rank"] as! Int
                    let duration = value["duration"] as! UInt

                    let typeString = value["type"] as! String
                    switch typeString {
                    case "track":
                        type = .track
                    default:
                        type = .album
                    }

                    if let artistDictionary = value["artist"] as? NSDictionary {
                        let name = artistDictionary["name"] as! String
                        let picture_small = artistDictionary["picture_small"] as! String
                        artist = Artist(name: name, picture_small: picture_small)
                    }

                    if let albumDictionary = value["album"] as? NSDictionary {
                        let title = albumDictionary["title"] as! String
                        var cover_small = ""
                        var cover_big = ""

                        if let value = albumDictionary["cover_small"] as? String {
                            cover_small = value
                        }
                        if let value = albumDictionary["cover_big"] as? String {
                            cover_big = value
                        }
                        album = Album(title: title, cover_small: cover_small, cover_big: cover_big)
                    }

                    if let artist = artist,  let album = album,  let type = type {
                        soundtracks.append(SoundTrackModel(trackId: trackId,
                                                        title: title,
                                                        type: type,
                                                        duration: duration,
                                                        rank: rank,
                                                        artist: artist,
                                                        album: album))
                    } else {
                        print("artist or album is nil for Track Dictionary")
                    }
                }
            } // forEach
        } // dataDictionary

        return soundtracks
    }

}
