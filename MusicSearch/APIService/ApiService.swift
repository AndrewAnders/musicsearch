//
//  ApiService.swift
//  MusicSearch
//
//  Created by Andrey Savitskiy on 19.03.2020.
//  Copyright © 2020 com.AndrewSV.MusicSearch. All rights reserved.
//

import Foundation
import Alamofire

class ApiService {

    static let sharedInstance = ApiService()

    private init() { }

    func fetchTracks(request:ApiRequestProtocol, completion:@escaping (Swift.Result<NSDictionary, Error>) -> Void) {
        if !NetworkReachabilityManager()!.isReachable {
            completion(.failure(ApiError.noConnection))
            return
        }

        let url = request.url()
        let headers = request.getHeaders()
        let parameters = request.parameters()

        AF.request(url, method: .get, parameters: parameters,
                          encoding: URLEncoding.default, headers: headers).responseJSON { response in

            switch response.result {
            case .success(let data):
                guard let dataDictionary = data as? NSDictionary else {
                    completion(.failure(ApiError.parseError))
                    return
                }

                completion(.success(dataDictionary))

            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
}

enum ApiError: Error {
    case noConnection
    case parseError
}
