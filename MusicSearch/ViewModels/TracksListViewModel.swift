//
//  TracksListViewModel.swift
//  MusicSearch
//
//  Created by Andrey Savitskiy on 19.03.2020.
//  Copyright © 2020 com.AndrewSV.MusicSearch. All rights reserved.
//

class TracksListViewModel {

    weak var view: TracksListViewProtocol?
    private var items: [SoundTrackModel] = []

    init(view: TracksListViewProtocol) {
        self.view = view
    }

    func tracks() -> [SoundTrackPresentable] {
        return items
    }

    func clearTracks() {
        items.removeAll()
    }

    func performSearch(searchText: String) {

        let searchRequest = ApiSearchRequest(search: searchText)
        ApiService.sharedInstance.fetchTracks(request: searchRequest) { result in
            switch result {
            case .success(let dataDictionary):
                let soundTracksManager = SoundTracksManager(trackDictionary: dataDictionary)
                self.items = soundTracksManager.serialize()
                self.view?.updateItemsList()

            case .failure(let error):
                self.view?.showError(error: error)
            }
        }
    }

    func trackFor(index: Int) -> SoundTrackModel {
        return items[index]
    }

}
