//
//  DetailViewModel.swift
//  MusicSearch
//
//  Created by Andrey Savitskiy on 23.03.2020.
//  Copyright © 2020 com.adsv.MusicSearch. All rights reserved.
//

import UIKit

class DetailViewModel {

    weak var view: DetailPresenterProtocol?
    let imageService = ImageService.sharedInstance

    init(view: DetailPresenterProtocol) {
        self.view = view
    }

    func getImageFor(trackModel: SoundTrackModel) {
        imageService.download(imageURL: trackModel.album.cover_big) { (result) in
            if case .success(let image) = result {
                self.view?.presentImage(image: image)
            }
        }
    }

}
